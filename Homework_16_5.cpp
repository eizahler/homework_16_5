#include <iostream>
#include <ctime>

int main()
{
	// ����������� �������
	const int N = 5;

	// ��������� �������� ����� ������
	time_t seconds = time(NULL);
	tm* timeinfo = localtime(&seconds);
	int day = timeinfo->tm_mday;

	// ������� ������� �������� ����� �� N
	int rem = day % N;

	// ������� � ��������� ������
	int matrix[N][N];
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			matrix[i][j] = i + j;
		}
	}

	// ������� ������ �� �����
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			std::cout << matrix[i][j] << ',';
		}
		std::cout << '\n';
	}

	// ������� ����� ��������� ������ ������� �� ������� rem
	std::cout << '\n';
	int sum = 0;
	for (int i = 0; i < N; i++)
	{
		sum += matrix[rem][i];
	}
	std::cout << "Rem = : " << rem << '\n';
	std::cout << "Sum = : " << sum;
}